#!/bin/sh

if [ ! -d /var/lib/mysql/mysql ]; then
    echo 'Initializing MySQL'
    mysql_install_db --user=root --verbose=0 > /dev/null

    echo "Initializing the database"
    tfile=`mktemp`
    [[ ! -f "$tfile" ]] && return 1
    cat << EOF > $tfile
USE mysql;
SET @@SESSION.SQL_LOG_BIN=0;
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'root' WITH GRANT OPTION;
DROP DATABASE test;
CREATE DATABASE IF NOT EXISTS corail CHARACTER SET utf8 COLLATE utf8_general_ci;
EOF
    cat /corail/server/migrations/* >> $tfile
    echo "INSERT INTO corail.user (roleId, firstname, lastname, email, password, createdAt) VALUES (NULL, 'Alexis', 'Fontaine', 'alexis.fontaine@outlook.com', 'alexis', '2016-05-18 19:31:14');" >> $tfile
    /usr/bin/mysqld --user=root --bootstrap --verbose=0 < $tfile
    rm -f $tfile
fi

/usr/bin/mysqld --user=root 2>&1 > /dev/null &

rm -rf /corail

exec /opt/tomcat/bin/catalina.sh run
