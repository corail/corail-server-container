# Corail - Server container

[![build status](https://gitlab.com/corail/corail-server-container/badges/master/build.svg)](https://gitlab.com/corail/corail-server-container/commits/master)

> Docker image to run the [Corail](https://gitlab.com/corail/corail) project.

The presentation website for Corail application: https://corail.gitlab.io.

You will find some information on how to run the application there.
