FROM davidcaste/alpine-tomcat:latest

MAINTAINER Alexis Fontaine

ARG BUILD_NUMBER

COPY startup.sh /

#
# Install and create MySQL database
#
RUN apk add --no-cache openssl unzip mysql mysql-client && \
    wget https://gitlab.com/corail/corail/builds/${BUILD_NUMBER}/artifacts/download -O corail.zip && \
    mkdir /corail && \
    unzip corail.zip -d /corail && \
    apk del openssl unzip
COPY my.cnf /etc/mysql/my.cnf
RUN chmod 644 /etc/mysql/my.cnf

#
# Install Wkhtmltopdf
#
RUN apk add --no-cache xvfb ttf-freefont fontconfig && \
    apk add wkhtmltopdf \
        --no-cache \
        --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
        --allow-untrusted && \
    mv /usr/bin/wkhtmltopdf /usr/bin/wkhtmltopdf-origin && \
    echo $'#!/usr/bin/env sh\n\
Xvfb :0 -screen 0 1024x768x24 -ac +extension GLX +render -noreset & \n\
DISPLAY=:0.0 wkhtmltopdf-origin $@ \n\
killall Xvfb\
' > /usr/bin/wkhtmltopdf && \
    chmod +x /usr/bin/wkhtmltopdf

#
# Retrieve WAR file
#
RUN cp /corail/server/target/out/corail.war ${TOMCAT_HOME}/webapps/ROOT.war && \
    rm -f /var/cache/apk/* corail.zip

EXPOSE 3306
CMD ["/bin/bash", "/startup.sh"]
